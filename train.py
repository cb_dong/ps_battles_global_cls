import os
import argparse
import json
import numpy as np
import sys
import time
import pdb
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.autograd import Variable
from tensorboardX import SummaryWriter


from common import DFL_DATA_PATH, DFL_CONFIG, DFL_DEVICE
from utils import logger, read_annotations, load_config, get_train_paths, Progbar, AverageMeter, predict_set, evaluate, collate_function
from models import build_net
from dataset import ImageDataset
from loss import FocalLoss


def validate(net, val_loader, runtime_params):
    probs, gt_labels, names = predict_set(net,val_loader, runtime_params)
    pred_labels = np.argmax(probs,axis=1)
    res = evaluate(gt_labels, pred_labels, probs[:,1])
    return res

def parse_args():
    parser = argparse.ArgumentParser(description='training')
    
    parser.add_argument('--data_path', type=str, default=DFL_DATA_PATH, help='path to datasets. (default: %s)'%DFL_DATA_PATH)
    parser.add_argument('--train_collection', type=str, help='training collection',required=True)
    parser.add_argument('--val_collection', type=str, help='validation collection',required=True)
    parser.add_argument('--annotation', type=str, help='annotation name',required=True)
    parser.add_argument('--config_name', type=str, default=DFL_CONFIG, help='model configuration file. (default: %s)'% DFL_CONFIG)
    parser.add_argument('--run_id', default=0, type=int, help='run_id (default: 0)')
    parser.add_argument('--resume', default=0, type=int, help='resume from an existing checkpoint (default: 0)')
    parser.add_argument('--device', default=DFL_DEVICE, type=str, help='cuda:n or cpu (default: %s)'% DFL_DEVICE)
    parser.add_argument('--overwrite', default=0, type=int, help='overwrite existing files (default: 0)')
    args = parser.parse_args()
    return args


def main(argv=None):
    opt = parse_args()
    print(json.dumps(vars(opt), indent=4))
    config = load_config('configs.{}'.format(opt.config_name))
    train_params = {
        'data_path':opt.data_path,
        'train_collection':opt.train_collection,
        'val_collection':opt.val_collection,
        'config_name':opt.config_name,
        'run_id':opt.run_id
    }
    device = torch.device(opt.device)
    
    model_dir, train_data_path, val_data_path = get_train_paths(opt)

    for data_path in [train_data_path, val_data_path]:
        if not os.path.exists(data_path):
            logger.error("{} does not exist".format(data_path))
            sys.exit(0)
        
    model_path = os.path.join(model_dir, "model.pth.tar")
    if os.path.exists(model_path):
        if opt.overwrite:
            logger.info('%s exists. overwrite' % model_path)
        else:
            logger.info('%s exists. stop' % model_path)
            sys.exit(0)
    elif not os.path.exists(model_dir):
        os.makedirs(model_dir)
   
    
    train_params_file = os.path.join(model_dir, 'train_params.json')
    with open(train_params_file, 'w') as fp:
        json.dump(train_params, fp, indent=4)
    
    train_set = ImageDataset(read_annotations(train_data_path), config.img_size, config.img_aug, config.balance)
    train_loader = DataLoader(
        dataset=train_set,
        num_workers=config.num_workers,
        batch_size=config.batch_size,
        pin_memory=True,
        shuffle=True,
        drop_last=True,
        collate_fn=collate_function
    )
    
    val_set = ImageDataset(read_annotations(val_data_path), config.img_size, False, False)
    val_loader = DataLoader(
        dataset=val_set,
        num_workers=config.num_workers,
        batch_size=config.batch_size,
        pin_memory=True,
        shuffle=True,
        drop_last=False,
        collate_fn=collate_function
    )

    writer = SummaryWriter(logdir=model_dir)
    net = build_net(config)
    if net is None:
        logger.error("{} not defined,please add it in models.build_net".format(config.net_name))
        sys.exit(0)

    perf_log_file = os.path.join(model_dir, "%s.txt" % config.metric)
    board_num = 0
    best_perf = 0.0
    no_impr_counter = 0 # for early stop
        
    if opt.resume and os.path.exists(model_path):
        net.load_state_dict(torch.load(model_path, map_location='cpu'))
        board_num, best_perf = open(perf_log_file).read().split()[:2]
        board_num = int(board_num)
        best_perf = float(best_perf)
            
    net = net.to(device)
    
    if config.loss == 'focal':
        loss_func = FocalLoss(gamma=config.focal_gama,alpha=config.focal_alpha)
    else:
        loss_func = nn.CrossEntropyLoss().to(device)
        
    optimizer = torch.optim.Adam(net.parameters(), lr=config.init_lr)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=8, gamma=0.9)
    logger.info("begin to train!")
    s_time = time.time()

    for epoch in range(config.max_epochs):
        net.train()
        progbar = Progbar(len(train_set), stateful_metrics=['epoch', 'config'])
        batch_time = AverageMeter()
        end = time.time()

        for batch_idx, (labels, imgs, img_paths) in enumerate(train_loader):
            if config.debug:
                logger.info('debug mode:begin to train\n')
                pdb.set_trace()

            optimizer.zero_grad()
            board_num += 1
            inputs = imgs.reshape((-1,3,imgs.size(-2), imgs.size(-1)))
            labels = labels.reshape((-1))

            inputs = Variable(inputs, requires_grad=True).to(device)
            labels = Variable(labels, requires_grad=False).to(device)
            output = net(inputs)

            loss = loss_func(output, labels)
            writer.add_scalars('ps_%s' % opt.config_name, {"train loss": loss.item()}, board_num)
            loss.backward()
            optimizer.step()
            scheduler.step()
            progbar.add(imgs.size(0), values=[('epoch', epoch), ('loss',loss.item())])
            batch_time.update(time.time() - end)
            end = time.time()


        #begin to validate every epoch
        #torch.save(net, model_path)
        
        res = validate(net, val_loader, {'device':device, 'run_type':'val'})
        
        if config.debug:
            logger.info('debug mode:first validation finished\n')
            pdb.set_trace()
        net.train()
        writer.add_scalars('ps_%s' % opt.config_name, {"test %s"%config.metric: res[config.metric]}, board_num)
        
        if res[config.metric] > best_perf:
            best_perf = res[config.metric] 
            torch.save(net, model_path)
            no_impr_counter = 0
        else:
            is_best = False
            no_impr_counter += 1
            
        logger.info('epoch %d -> val %s: %.4f, best %s: %.4f' % ( epoch, config.metric, res[config.metric], config.metric, best_perf))
        open(perf_log_file, 'w').write('{} {}'.format(board_num, best_perf))
        
        if no_impr_counter > config.early_stop_bar:
            logger.info('Early stop')
            break

            
    time_span = time.time() - s_time
    logger.info("training done in {} minutes".format(time_span/60.0) )
    
if __name__ == '__main__':
    main()





