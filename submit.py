import os
import argparse
import json
import sys

from utils import logger

def parse_args():
    parser = argparse.ArgumentParser(description='submission')
    parser.add_argument('--pred_file', type=str, help='prediction file', required=True)

    args = parser.parse_args()
    return args

def main(argv=None):
    opt = parse_args()
    print(json.dumps(vars(opt), indent=4))
    readable = ['real', 'fake']
    res_file = opt.pred_file + '.ffb.json'
    logger.info(res_file)

    res = {}
    nr_pos = 0

    for line in open(opt.pred_file).readlines():
        img_path, pred, score = line.split()
        pred = int(pred) #int(float(score)>0.)
        img_name = os.path.split(img_path)[-1]
        res[img_name] = readable[pred]
        nr_pos += pred
    logger.info("{} predicted pos".format(nr_pos))

    with open(res_file, 'w') as fp:
        json.dump(res, fp, indent=4, ensure_ascii=False, sort_keys=True)

if __name__ == '__main__':
    main()
    



