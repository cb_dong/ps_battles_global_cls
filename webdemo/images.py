import web
import os
import json


cType = {"png":"images/png",
         "jpg":"images/jpeg",
         "jpeg":"images/jpeg",
         "gif":"images/gif",
         "ico":"images/x-icon"}

DEFAULT_EXT = 'jpg'


class images:
    def get_local(self, name):
        img_path = web.img2info[name]['img_path']
        #print (img_path)
        return img_path

    def GET(self,name):
        ext = name.split(".")[-1] # Gather extension
        if name.find('.')<0:
            ext = DEFAULT_EXT
        imfile = self.get_local(name)
        try:
            web.header("Content-Type", cType[ext]) # Set the Header
            return open(imfile,"rb").read() # Notice 'rb' for reading images
        except:
            raise web.notfound()

class gradcams(images):
    def get_local(self, name):
        img_path = web.img2info[name]['gradcam']
        print (img_path)
        return img_path

class gradcampps(images):
    def get_local(self, name):
        img_path = web.img2info[name]['gradcampp']
        #print (img_path)
        return img_path

class rawimg(images):
    def get_local(self, name):
        img_path = web.img2info[name]['rawimg']
        return img_path

class queryimages (images):
    def get_local(self, name):
        #print im2path(testCollection, name, True)
        return im2path(testCollection, name, True)
            
class bigimages (images):
    def get_local(self, name):
        return im2path(name, True)

            
if __name__ == '__main__':
    im = bigimages()
    im.GET('4362444639.jpg')

