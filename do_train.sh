source common.ini

python train.py --data_path $data_path --train_collection $train_collection --annotation $annotation --val_collection $val_collection --run_id $run_id --config_name $config_name --device $device --overwrite $overwrite

