# -*- coding: utf-8 -*-
"""
Created on 2019/8/4 上午9:53

@author: mick.yi

入口类

"""
import argparse
import os
import re
import json
import pdb
import time

import cv2
import numpy as np
import torch
from torch.utils.data import DataLoader
from torchvision.transforms import ToPILImage
from dataset import ImageDataset
from common import DFL_DATA_PATH, DFL_CONFIG, DFL_DEVICE
from utils import logger, collate_function, Progbar, AverageMeter, load_config

from interpretability.grad_cam import GradCAM, GradCamPlusPlus
from interpretability.guided_back_propagation import GuidedBackPropagation
from interpretability.utils import  get_last_conv_name,gen_cam,norm_image,gen_gb,save_image

def parse_args():
    parser = argparse.ArgumentParser(description='draw gradcam')
    parser.add_argument('--data_path', type=str, default=DFL_DATA_PATH,
                        help='path to datasets. (default: %s)' % DFL_DATA_PATH)
    parser.add_argument('--draw_list', type=str, help='txt files for drawing,relative path to data_path', required=True)
    parser.add_argument('--config_name', type=str, default=DFL_CONFIG,help='model configuration file. (default: %s)' % DFL_CONFIG)
    parser.add_argument('--model_dir', type=str, help='model_dir', required=True)
    parser.add_argument('--save_dir', type=str, help='path to save heatmap,relative to data_path', required=True)
    #parser.add_argument('--img_size', default=224, type=int, help='resize scale')
    parser.add_argument('--device', default=DFL_DEVICE, type=str, help='cuda:n or cpu (default: %s)' % DFL_DEVICE)
    args = parser.parse_args()
    return args


def main(argv=None):
    opt = parse_args()
    print(json.dumps(vars(opt), indent=4))

    config = load_config('configs.{}'.format(opt.config_name))
    print("img_size: ",config.img_size)
    model_path = os.path.join(opt.data_path, opt.model_dir)
    save_dir = os.path.join(opt.data_path, opt.save_dir)

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    
    draw_data_path = os.path.join(opt.data_path, opt.draw_list)
    draw_samples = [x.strip() for x in open(draw_data_path).readlines() if x.strip()]
    annotations = [(x, 0) for x in draw_samples]
    draw_set = ImageDataset(annotations, img_size=config.img_size, aug=False, balance=False)
    draw_loader = DataLoader(
        dataset=draw_set,
        num_workers=0,
        batch_size=1,
        pin_memory=True,
        shuffle=False,
        drop_last=False,
        collate_fn=collate_function
    )

    net = torch.load(model_path, map_location='cpu').to(opt.device)
    #net = torchvision.models.resnet50(pretrained=True).to(opt.device)
    #layer_name = 'layer4'
    layer_name = get_last_conv_name(net)
    logger.info("layer name: %s"%layer_name)
    grad_cam = GradCAM(net, layer_name, config.img_size)
    grad_cam_plus_plus = GradCamPlusPlus(net, layer_name, config.img_size)
    gbp = GuidedBackPropagation(net)

    progbar = Progbar(len(draw_set))
    batch_time = AverageMeter()
    end = time.time()

    for inner, (labels, inputs, name) in enumerate(draw_loader):
        name = name[0]
        img = inputs[0]
        img = ToPILImage()(img * 0.5 + 0.5)
        img = cv2.cvtColor(np.asarray(img), cv2.COLOR_RGB2BGR) / 255.0
        inputs = inputs.to(opt.device)
        inputs = inputs.requires_grad_(True)

        # 输出图像
        image_dict = {}

        # Grad-CAM
        mask = grad_cam(inputs, None)  # cam mask
        image_dict['cam'], image_dict['heatmap'] = gen_cam(img, mask)
        grad_cam.remove_handlers()

        # Grad-CAM++
        mask_plus_plus = grad_cam_plus_plus(inputs, None)  # cam mask
        image_dict['campp'], image_dict['heatmappp'] = gen_cam(img, mask_plus_plus)
        grad_cam_plus_plus.remove_handlers()

        # GuidedBackPropagation
        inputs.grad.zero_()  # 梯度置零
        grad = gbp(inputs)
        gb = gen_gb(grad)
        image_dict['gb'] = norm_image(gb)

        # 生成Guided Grad-CAM
        cam_gb = gb * mask[..., np.newaxis]
        image_dict['cam_gb'] = norm_image(cam_gb)

        save_image(image_dict, os.path.split(name)[-1], save_dir)
        progbar.add(1, values=[('batch_time', batch_time.val)])
        batch_time.update(time.time() - end)
        end = time.time()

if __name__ == '__main__':
    main()
