import torch
import torch.nn as nn
import torchvision
from torch.utils.data import Dataset
import torchvision.transforms as transforms
from torch.autograd import Variable
from PIL import Image
import cv2
import pdb
import numpy as np

class ImageDataset(Dataset):
    def __init__(self, annotations, img_size=(224,224), aug=True, balance=False):
        self.img_size = img_size
        self.aug = aug
        self.balance = balance
        if balance:
            self.data = [[x for x in annotations if x[1] == lab] for lab in [0,1]]
        else:
            self.data = [annotations]

        self.norm_transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
        ])
        self.aug_transform = transforms.Compose([
            transforms.RandomHorizontalFlip(p=np.random.randint(0, 2)),
            transforms.RandomVerticalFlip(p=np.random.randint(0, 2)),
            transforms.RandomRotation(3 * np.random.randint(0, 11)),
            transforms.RandomGrayscale(p=0.5),
            transforms.ColorJitter(brightness=np.random.random(),
                                   contrast=np.random.random(),
                                   saturation=np.random.random(),
                                   hue=0.5 * np.random.random())
        ])

    def __len__(self):
        return max([len(subset) for subset in self.data])

    def __getitem__(self, index):
        if self.balance:
            safe_idx = index % len(self.data[0])
            img_path_neg = self.data[0][safe_idx][0]
            img_neg = self.load_sample(img_path_neg)
            lab_neg = self.data[0][safe_idx][1]

            safe_idx = index % len(self.data[1])
            img_path_pos = self.data[1][safe_idx][0]
            img_pos = self.load_sample(img_path_pos)
            lab_pos = self.data[1][safe_idx][1]
            return torch.tensor([lab_neg, lab_pos]), torch.cat((img_neg.unsqueeze(0), img_pos.unsqueeze(0))), [
                img_path_neg, img_path_pos]
        else:
            lab = self.data[0][index][1]
            img_path = self.data[0][index][0]
            img = self.load_sample(img_path)
            lab = torch.tensor(lab,dtype=torch.long)
            return lab, img, img_path
   
    def load_sample(self, img_path):
        img = Image.open(img_path)
        #img = cv2.imread(img_path)
        try:
            img = img.resize(self.img_size)
            #img = cv2.resize(img, self.img_size)
            if self.aug:
                #img = Image.fromarray(cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
                img = self.aug_transform(img) #img： PIL.Image格式
                #img = cv2.cvtColor(np.asarray(img), cv2.COLOR_RGB2BGR) #RGB2BGR
            img = self.norm_transform(img)
        except:
            pdb.set_trace()
        return img
