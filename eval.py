import os
import argparse
import json
import sys
import pdb
import numpy as np

from common import DFL_DATA_PATH
from utils import logger, load_config, get_test_paths, evaluate


def parse_args():
    parser = argparse.ArgumentParser(description='evaluation')
    parser.add_argument('--data_path', type=str, default=DFL_DATA_PATH, help='path to datasets. (default: %s)' % DFL_DATA_PATH)
    parser.add_argument('--test_collection', type=str, help='testing collection', required=True)
    parser.add_argument('--annotation', type=str, help='annotation name', required=True)
    parser.add_argument('--pred_dir', type=str, help='pred_dir', required=True)

    args = parser.parse_args()
    return args

def main(argv=None):
    opt = parse_args()
    print(json.dumps(vars(opt), indent=4))

    pred_file = os.path.join(opt.data_path, opt.test_collection, 'pred', opt.pred_dir, 'results.txt')
    gt_file = os.path.join(opt.data_path, opt.test_collection, 'annotations', opt.annotation, '%s.txt' % opt.test_collection)
    logger.info("pred file: %s\n gt file: %s"%(pred_file,gt_file))
    img2lab = {}
    for line in open(gt_file).readlines():
        img, lab = line.strip().split()
        img2lab[img] = int(lab)
        
    labels = []
    preds = []
    probs = []
    
    for line in open(pred_file).readlines():
        elems = line.strip().split()
        img, pred, prob = elems
        gt = img2lab[img]
        pred = int(pred)
        prob = float(prob)
        
        labels.append(gt)
        preds.append(pred)
        probs.append(prob)

    res = evaluate(labels, preds, probs)
    metrics = sorted(res.keys())
    print (' '.join(metrics))
    print (' '.join(['%.4f'%res[x] for x in metrics]))
        

if __name__ == '__main__':
    main()
