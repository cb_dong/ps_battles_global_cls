import cv2
import pdb
import argparse
import os
import json
from common import DFL_DATA_PATH, DFL_CONFIG, DFL_DEVICE
from tqdm import tqdm

def parse_args():
    parser = argparse.ArgumentParser(description='predict')
    parser.add_argument('--data_path', type=str, default=DFL_DATA_PATH,
                        help='path to datasets. (default: %s)' % DFL_DATA_PATH)
    parser.add_argument('--img_list', type=str, help='txt files for pre_processing,relative path to data_path',required=True)
    parser.add_argument('--save_dir', type=str, help='path to save resized img,relative to data_path', required=True)
    parser.add_argument('--img_size', default=448, type=int, help='resize scale,0 for no resize')
    args = parser.parse_args()
    return args

def main(argv=None):
    pdb.set_trace()
    opt = parse_args()
    print(json.dumps(vars(opt), indent=4))
    with open(os.path.join(opt.data_path,opt.img_list), 'r') as f:
        data = f.readlines()
    dir_name = '%s_%dx%d' %(opt.img_list.split('/')[-1].split('.')[0],opt.img_size,opt.img_size)
    full_dir_name = os.path.join(opt.save_dir,dir_name)
    if not os.path.exists(full_dir_name):
        os.makedirs(full_dir_name)
    with open(dir_name+'.txt','w') as f:

        for each in tqdm(data):
            name = each.split(' ')[0].split('/')[-1].split('.')[0]
            label = each.split(' ')[1]
            each = each.split(' ')[0]
            img = cv2.imread(each)
            img = cv2.resize(img, (opt.img_size, opt.img_size))
            cv2.imwrite(os.path.join(dir_name,name)+'.png',img)
            f.write("%s %s"%(os.path.join(dir_name,name)+'.png',label))


if __name__ == '__main__':
    main()