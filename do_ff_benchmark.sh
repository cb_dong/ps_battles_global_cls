source common.ini

test_collection=faceforensics_benchmark
test_collection=faceforensics_benchmark_retina_face

model_dir=$train_collection/models/$val_collection/$annotation/$config_name/run_$run_id

python predict.py --data_path $data_path --test_collection $test_collection  --model_dir $model_dir --device $device --overwrite $overwrite

pred_file=$data_path/$test_collection/pred/$train_collection/$val_collection/$annotation/$config_name/run_$run_id/results.txt

python submit.py --pred_file $pred_file
