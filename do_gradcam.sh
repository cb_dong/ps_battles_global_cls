source common.ini

#test_collection=faceforensics_benchmark_retina_face

model_dir=$train_collection/models/$val_collection/$annotation/$config_name/run_$run_id/model.pth.tar
draw_list=$test_collection/$test_collection.txt
save_dir=$test_collection/pred/$train_collection/$val_collection/$annotation/$config_name/run_$run_id/gradcam
config_name=$config_name

python draw_gradcam.py --data_path $data_path --draw_list $draw_list  --save_dir $save_dir --model_dir $model_dir --device $device --config_name $config_name
