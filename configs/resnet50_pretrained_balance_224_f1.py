class Config(object):
    init_lr = 1e-3
    max_epochs = 100
    board_num = 0
    max_acc = 0
    debug = 0
    img_aug = True
    img_size = (224, 224)
    pre_trained = True
    val_interval = 100
    if debug:
        batch_size = 4
        num_workers = 0
    else:
        batch_size = 100
        num_workers = 4

    loss = 'balance'
    if loss == 'focal':
        focal_alpha = 0.15
        focal_gama = 1
        balance = False
    else:
        balance = True

    net_name = 'resnet50'
    early_stop_bar = 10
    metric = 'f1'

