import torch
import torch.nn as nn
import torchvision
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torchvision.transforms as transforms
from torch.autograd import Variable
from PIL import Image
import time
import numpy as np
import os
import cv2
import pdb
import collections
import pickle
import warnings
import gc as g_c
import torch.nn.functional as F
import random
import importlib
import sys
import json
from sklearn.metrics import roc_auc_score, average_precision_score, confusion_matrix

import logging

logger = logging.getLogger(__file__)
logging.basicConfig(
    format="[%(asctime)s - %(filename)s:line %(lineno)s] %(message)s",
    datefmt='%d %b %H:%M:%S',
    level=logging.INFO)

def collate_function(data):
    transposed_data = list(zip(*data))
    lab, img, img_path = transposed_data[0], transposed_data[1], transposed_data[2]
    img = torch.stack(img, 0)
    lab = torch.stack(lab, 0)
    return lab, img, img_path


def load_pretrained(pretrained,target):
    pretrained_dict = pretrained.state_dict()
    model_dict = target.state_dict()
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
    model_dict.update(pretrained_dict)
    target.load_state_dict(model_dict)
    return target


def load_config(config_path):
    module = importlib.import_module(config_path)
    return module.Config()


def get_train_paths(args):
    train_data_path = os.path.join(args.data_path, args.train_collection, "annotations", args.annotation, args.train_collection + ".txt")
    val_data_path = os.path.join(args.data_path, args.val_collection, "annotations", args.annotation, args.val_collection + ".txt")
    model_dir = os.path.join(args.data_path, args.train_collection, "models", args.val_collection, args.annotation, args.config_name, "run_%d"%args.run_id)
    
    return [model_dir, train_data_path, val_data_path]


def read_annotations(data_path):
    lines = map(str.strip, open(data_path).readlines())
    data = []
    for line in lines:
        sample_path, label = line.split()
        label = int(label)
        data.append((sample_path, label))
    return data

def get_test_paths(args):
    test_data_path = os.path.join(args.data_path, args.test_collection, args.test_collection + ".txt")
    pred_dir = os.path.join(args.data_path, args.test_collection, "pred", args.model_dir.replace('/models/', '/'))
    return test_data_path, pred_dir


def evaluate(gt_labels, pred_labels, scores):
    n = len(gt_labels)
    tn, fp, fn, tp = confusion_matrix(gt_labels, pred_labels).reshape(-1)
    assert((tn + fp + fn + tp) == n)
    
    auc = roc_auc_score(gt_labels, scores)
    ap = average_precision_score(gt_labels, scores)
    sen = float(tp) / (tp + fn)
    spe = float(tn) / (tn + fp)
    f1 = 2.0*sen*spe / (sen + spe)
    acc = float(tn + tp) / n
    return {'auc':auc, 'ap':ap, 'sen':sen, 'spe':spe, 'f1':f1, 'acc':acc}

    
def caculate(pred,true,th):
    pred_ = (pred > th)
    P = np.sum(true)
    TP = np.sum(pred_ * true)
    TN = np.sum(pred_ == true) - TP
    N = np.sum(true == 0)
    FN = P - TP
    FP = N - TN
    return TP,TN,FP,FN#acc,precision,recall


def predict_set(net, dataloader, runtime_params):
    device = runtime_params['device']
    run_type = runtime_params['run_type']
    net = net.eval()
    progbar = Progbar(len(dataloader.dataset), stateful_metrics=['run-type'])
    batch_time = AverageMeter()
    names = []
    probs = np.array([])
    gt_labels = np.array([])
    with torch.no_grad():
        for i, (labels, imgs, img_paths) in enumerate(dataloader):
            s_time = time.time()
            imgs = imgs.to(device)
            names.extend(img_paths)
            output = net(imgs)
            output = torch.softmax(output, dim=1).cpu().numpy()

            probs = np.concatenate(probs,output)
            gt_labels = np.concatenate(gt_labels,labels.data.numpy())

            progbar.add(imgs.size(0), values=[('run-type', run_type)])  # ,('batch_time', batch_time.val)])
            batch_time.update(time.time() - s_time)
    return probs, gt_labels, names

def smoke_testing():
    data_path = os.path.join(os.environ['HOME'], 'VisualSearch3')
    train_collection = 'ps-battles_dcb_train'
    train_data_path = os.path.join(data_path, train_collection, 'annotations', '%s.txt'%train_collection)
    data = read_annotations(train_data_path)
    nr_pos = len([x for x in data if x[1] == 1])
    nr_neg = len([x for x in data if x[1] == 0])
    assert( (nr_pos + nr_neg) == len(data) )
    print('pos {}, neg {}'.format(nr_pos, nr_neg))


class Progbar(object):
    """Displays a progress bar.
    # Arguments
        target: Total number of steps expected, None if unknown.
        width: Progress bar width on screen.
        verbose: Verbosity mode, 0 (silent), 1 (verbose), 2 (semi-verbose)
        stateful_metrics: Iterable of string names of metrics that
            should *not* be averaged over time. Metrics in this list
            will be displayed as-is. All others will be averaged
            by the progbar before display.
        interval: Minimum visual progress update interval (in seconds).
    """

    def __init__(self, target, width=30, verbose=1, interval=0.05,
                 stateful_metrics=None):
        self.target = target
        self.width = width
        self.verbose = verbose
        self.interval = interval
        if stateful_metrics:
            self.stateful_metrics = set(stateful_metrics)
        else:
            self.stateful_metrics = set()

        self._dynamic_display = ((hasattr(sys.stdout, 'isatty') and
                                  sys.stdout.isatty()) or
                                 'ipykernel' in sys.modules)
        self._total_width = 0
        self._seen_so_far = 0
        self._values = collections.OrderedDict()
        self._start = time.time()
        self._last_update = 0

    def update(self, current, values=None):
        """Updates the progress bar.
        # Arguments
            current: Index of current step.
            values: List of tuples:
                `(name, value_for_last_step)`.
                If `name` is in `stateful_metrics`,
                `value_for_last_step` will be displayed as-is.
                Else, an average of the metric over time will be displayed.
        """
        values = values or []
        for k, v in values:
            if k not in self.stateful_metrics:
                if k not in self._values:
                    self._values[k] = [v * (current - self._seen_so_far),
                                       current - self._seen_so_far]
                else:
                    self._values[k][0] += v * (current - self._seen_so_far)
                    self._values[k][1] += (current - self._seen_so_far)
            else:
                self._values[k] = v
        self._seen_so_far = current

        now = time.time()
        info = ' - %.0fs' % (now - self._start)
        if self.verbose == 1:
            if (now - self._last_update < self.interval and
                    self.target is not None and current < self.target):
                return

            prev_total_width = self._total_width
            if self._dynamic_display:
                sys.stdout.write('\b' * prev_total_width)
                sys.stdout.write('\r')
            else:
                sys.stdout.write('\n')

            if self.target is not None:
                numdigits = int(np.floor(np.log10(self.target))) + 1
                barstr = '%%%dd/%d [' % (numdigits, self.target)
                bar = barstr % current
                prog = float(current) / self.target
                prog_width = int(self.width * prog)
                if prog_width > 0:
                    bar += ('=' * (prog_width - 1))
                    if current < self.target:
                        bar += '>'
                    else:
                        bar += '='
                bar += ('.' * (self.width - prog_width))
                bar += ']'
            else:
                bar = '%7d/Unknown' % current

            self._total_width = len(bar)
            sys.stdout.write(bar)

            if current:
                time_per_unit = (now - self._start) / current
            else:
                time_per_unit = 0
            if self.target is not None and current < self.target:
                eta = time_per_unit * (self.target - current)
                if eta > 3600:
                    eta_format = '%d:%02d:%02d' % (eta // 3600, (eta % 3600) // 60, eta % 60)
                elif eta > 60:
                    eta_format = '%d:%02d' % (eta // 60, eta % 60)
                else:
                    eta_format = '%ds' % eta

                info = ' - ETA: %s' % eta_format
            else:
                if time_per_unit >= 1:
                    info += ' %.0fs/step' % time_per_unit
                elif time_per_unit >= 1e-3:
                    info += ' %.0fms/step' % (time_per_unit * 1e3)
                else:
                    info += ' %.0fus/step' % (time_per_unit * 1e6)

            for k in self._values:
                info += ' - %s:' % k
                if isinstance(self._values[k], list):
                    avg = np.mean(
                        self._values[k][0] / max(1, self._values[k][1]))
                    if abs(avg) > 1e-3:
                        info += ' %.4f' % avg
                    else:
                        info += ' %.4e' % avg
                else:
                    info += ' %s' % self._values[k]

            self._total_width += len(info)
            if prev_total_width > self._total_width:
                info += (' ' * (prev_total_width - self._total_width))

            if self.target is not None and current >= self.target:
                info += '\n'

            sys.stdout.write(info)
            sys.stdout.flush()

        elif self.verbose == 2:
            if self.target is None or current >= self.target:
                for k in self._values:
                    info += ' - %s:' % k
                    avg = np.mean(
                        self._values[k][0] / max(1, self._values[k][1]))
                    if avg > 1e-3:
                        info += ' %.4f' % avg
                    else:
                        info += ' %.4e' % avg
                info += '\n'

                sys.stdout.write(info)
                sys.stdout.flush()

        self._last_update = now

    def add(self, n, values=None):
        self.update(self._seen_so_far + n, values)


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / (.0001 + self.count)

    def __str__(self):
        """String representation for logging
        """
        # for values that should be recorded exactly e.g. iteration number
        if self.count == 0:
            return str(self.val)
        # for stats
        return '%.4f (%.4f)' % (self.val, self.avg)

if __name__ == '__main__':
    smoke_testing()
